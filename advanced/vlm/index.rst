##########################
 VideoLAN Manager
##########################

.. toctree::
   :maxdepth: 2

   introduction.rst
   vlm_broadcast_playlist.rst
   vlm_three_streams.rst
   vlm_share_screen.rst
   vlm_share_screen_with_logo.rst
   vlm_mosaic.rst
