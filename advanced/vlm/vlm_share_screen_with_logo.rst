.. _vlm-share-screen-with-logo:

#########################################
VLM Example: Share Screen with A Logo
#########################################

This page explains how to share your screen over the local network with a logo attached to the screen recording. Refer to :ref:`Share Screen using VLM <vlm-share-screen>` to see how to perform a simple screen share broadcast using VLM. 

**********************************
How to add a logo to the stream?
**********************************

We can add logo(s) to any stream by using the ``logo`` sfilter in the ``transcode`` block. Further, we can customize the logo by specifying the settings of various parameters like:

 + ``file``: Used to specify the path and file name of the image that will be used as the logo.
 + ``opacity``: Used for controlling the transparency of the logo (Minimum value = 0, Maximum value = 255).
 + ``position``: Used to specify the base position where the logo should appear on the stream.
 + ``x`` and ``y``: Used to specify the offsets (in pixels) from the base position. 

For example, to add an image saved by the name of ``logo.png`` at the bottom right corner of the screen at 75% opacity, we will use the following code snippet:

.. code-block ::

   #transcode{..., sfilter=logo{file="C:\path\to\file\logo.png",opacity=192,position=10}}


+++++++++++++++++++++++++++++
Setting the VLM File
+++++++++++++++++++++++++++++

We know how to share the screen using a VLM file, and how to add a logo to any stream. By combining these to, we will be able to share the screen with a logo attached to it.

Consider the following code: 

.. code-block ::

   new screen_share broadcast enabled
   setup screen_share input "screen://"
   setup screen_share output #transcode{vcodec=mpgv,sfilter=logo{file="C:\Users\UserName\Desktop\cone.png",opacity=192,position=10,x=20,y=40}}:udp{mux=ts,dst=239.255.255.250:8090,sap,name=streaming}
   setup screen_share option sout-keep
   setup screen_share option screen-fps=100
   setup screen_share option file-caching=1000
   control screen_share play

For the above code:

 + Create a new file in any text editor and copy the code.
 + Change the path of the logo image in the ``file`` parameter (use ``\`` on Windows and ``/`` on MacOS and Linux based systems).
 + Save the file with the ``.vlm`` exension (like ``screen_share.vlm``).


++++++++++++++++++++++++++++++++++++
Start broadcasting your screen
++++++++++++++++++++++++++++++++++++

On the device whose screen is to streamed, follow these steps:

 + Open the terminal and navigate to the directory where you saved the ``.vlm`` file.
 + Run the following command:

.. code-block ::

   $ vlc --vlm-conf=screen_share.vlm

As soon as you execute the above line, a VLC window will open and start streaming your screen to the local network with the logo attached to it.


++++++++++++++++++++++++++++++++++++
Receiving the screen recording
++++++++++++++++++++++++++++++++++++

The way to receive the recording is the same as mentioned in :ref:`Receiving the screen share stream <vlm-receive-screen-record>`.  

To receive the recording, simply open the VLC Media Player on the receiving device(s) and follow these steps:

 + Navigate to :menuselection:`View -> Playlist`
 + Click on ``Network Streams (SAP)`` which is in the ``Local Network`` section on the left side.
 + You will see a SAP stream by the name ``streaming``. Click on it to begin receiving.


As an example, this is how the screen recordings look like when they are streamed with and without the logo:

.. figure::  /images/advanced/vlm/vlm_share_screen_with_logo_without_logo.png
   :align:   center

   A snippet from a simple screen recording without any logo.

.. figure::  /images/advanced/vlm/vlm_share_screen_with_logo_with_logo.png
   :align:   center

   Screen recording with Logo (on the bottom right corner).

|

***************************************
How to switch between multiple logos?
***************************************

To switch between multiple logos, we can use the following the parameters:

 + In the ``file`` parameter, specify a list of images that are to be used as the logos (spearated with semicolons).
 + Add a parameter ``delay``, which is used to specify how long one logo should be shown before it is switched with the next one on the list.
 + Add a parameter ``repeat``, which is used to specify how many times we go back to the first image when the stream reaches the last image. 


Hence, if you wish to switch between three logos ``logo1.png``, ``logo2.png``, and ``logo3.png``, with a delay of 10 seconds, replace the third line of the vlm file with the following:

.. code-block ::

   setup screen_share output #transcode{vcodec=mpgv,sfilter=logo{file="C:\Users\UserName\Desktop\logo1.png;C:\Users\UserName\Desktop\logo2.png;C:\Users\UserName\Desktop\logo3.png",delay=10000,repeat=10,opacity=192,position=10,x=20,y=40}}:udp{mux=ts,dst=239.255.255.250:8090,sap,name=streaming}

Notice that the ``delay`` is set to 10000. This is because the value of ``delay`` is treated to be in milli-seconds (ms). 